package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    private int number;
    private Student classLeader;
    private List<Student> students = new ArrayList<>();
    private List<Teacher> teachers = new ArrayList<>();
    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        Klass klass = (Klass)o;
        return this.number == klass.number;
    }

    public void assignLeader(Student student) {
        if (student.isIn(this)) {
            classLeader = student;
            for (Teacher teacher : teachers) {
                if (teacher.belongsTo(this)) {
                    // fix klassList
                    System.out.println("I am "+teacher.getName() + ", teacher of Class " + teacher.getKlassList().get(0).getNumber()
                            + ". I know " + student.getName() + " become Leader.");

                }
            }
            for (Student stu : students) {
                if (stu.getName() != student.getName()) {
                    System.out.println("I am "+stu.getName()+", student of Class "+
                            stu.getKlass().getNumber()+". I know "+student.getName()+" become Leader.");
                }
            }
        } else {
            System.out.println("It is not one of us.");
        }
    }

    public boolean isLeader(Student student) {
        if (student == null) {
            return false;
        }
        if (this.classLeader == null) {
            return false;
        }
        return classLeader.getId() == student.getId();
    }


    public void attach(Person person) {
        if (person instanceof Teacher) {
            this.teachers.add((Teacher) person);
        }
    }

    public void addStudent(Student student) {
        students.add(student);
    }
}
