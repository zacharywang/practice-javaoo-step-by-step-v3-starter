package ooss;

public class Student extends Person {
    private Klass klass;
    public Student(int id, String name, int age) {
        super(id, name, age);
    }
    public void join(Klass klass){
        this.klass=klass;
        klass.addStudent(this);
    }
    public boolean isIn(Klass klass) {
        if (this.klass == null) {
            return false;
        }
        return this.klass.getNumber()==klass.getNumber();
    }
    @Override
    public String introduce() {
        if (klass != null && klass.isLeader(this)) {
            return super.introduce() + " I am a student. I am the leader of class " + this.klass.getNumber() + ".";
        } else {
            if (klass == null) {
                return super.introduce() + " I am a student.";
            }
            return super.introduce() + " I am a student. I am in class " + this.klass.getNumber() + ".";
        }
    }

    public Klass getKlass() {
        return klass;
    }
}
