package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private final List<Klass> klassList = new ArrayList<>();

    public List<Klass> getKlassList() {
        return klassList;
    }

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    public boolean belongsTo(Klass klass){
        if (klass == null) {
            return false;
        }
        return klassList.contains(klass);
    }

    public void assignTo(Klass klass){
        if (klass == null) {
            return;
        }
        klassList.add(klass);
    }

    @Override
    public String introduce() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.introduce())
                .append(" I am a teacher.");
        if (klassList.size() != 0) {
            sb.append(" I teach Class ");
            klassList.forEach(klass -> {
                sb.append(klass.getNumber());
                int klassIndex = klassList.indexOf(klass);
                if (klassIndex != klassList.size() - 1) {
                    sb.append(", ");
                }
            });
            sb.append(".");
        }

        return sb.toString();
    }

    public boolean isTeaching(Student student) {
        return klassList.stream().anyMatch(klass -> {
            if (student != null) {
                return student.isIn(klass);
            }
            return false;
        });
    }
}
